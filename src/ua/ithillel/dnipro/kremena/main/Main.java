package ua.ithillel.dnipro.kremena.main;

import ua.ithillel.dnipro.kremena.task1.view.UserConsole;

public class Main {

    public static void main(String[] args) {

        System.out.println("--------------------------------------------------------------------------------");
        System.out.println("Homework 2 | task 1 | begin");
        UserConsole.userDialog();
        System.out.println("Homework 2 | task 1 | end");
        System.out.println("--------------------------------------------------------------------------------");

    }
}