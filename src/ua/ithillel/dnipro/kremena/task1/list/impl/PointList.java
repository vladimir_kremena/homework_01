package ua.ithillel.dnipro.kremena.task1.list.impl;

import ua.ithillel.dnipro.kremena.task1.entity.Point;
import ua.ithillel.dnipro.kremena.task1.list.List;

import java.util.Arrays;

public class PointList implements List {

    private Point[] points = {};

    @Override
    public void add(Point point) {
        points = Arrays.copyOf(points, size() + 1);
        points[size() - 1] = point;
    }

    @Override
    public Point get(int index) {
        return points[index];
    }

    @Override
    public int size() {
        return points.length;
    }

    @Override
    public String toString() {
        return  "PointList [" +
                "points = " + Arrays.toString(points) +
                ']';
    }
}