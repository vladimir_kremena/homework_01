package ua.ithillel.dnipro.kremena.task1.list;

import ua.ithillel.dnipro.kremena.task1.entity.Point;

public interface List {

    void add(Point point);

    Point get(int index);

    int size();

}