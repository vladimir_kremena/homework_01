package ua.ithillel.dnipro.kremena.task1.view;

import ua.ithillel.dnipro.kremena.task1.services.CircleService;
import ua.ithillel.dnipro.kremena.task1.services.PointService;
import ua.ithillel.dnipro.kremena.task1.services.imp.InMemoryCircleService;
import ua.ithillel.dnipro.kremena.task1.services.imp.InMemoryPointService;
import ua.ithillel.dnipro.kremena.task1.view.menu.Menu;
import ua.ithillel.dnipro.kremena.task1.view.menu.MenuItem;
import ua.ithillel.dnipro.kremena.task1.view.menu.imp.*;

import java.util.Scanner;

public class UserConsole {

    public static void userDialog() {

        Scanner scanner = new Scanner(System.in);
        PointService pointService = new InMemoryPointService();
        CircleService circleService = new InMemoryCircleService();

        Menu menu = new Menu(scanner, new MenuItem[] {
                new AddPointMenuItem(scanner, pointService),
                new ChangeCircleMenuItem(scanner, circleService),
                new ContainsPointMenuItem(pointService, circleService),
                new NotContainsPointMenuItem(pointService, circleService),
                new ExitMenuItem()
        });
        menu.run();
    }
}