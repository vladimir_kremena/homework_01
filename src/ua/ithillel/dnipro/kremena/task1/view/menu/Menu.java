package ua.ithillel.dnipro.kremena.task1.view.menu;

import java.util.Scanner;

public class Menu {

    private Scanner scanner;
    private MenuItem[] menuItems;

    public Menu(Scanner scanner, MenuItem[] menuItems) {
        this.scanner = scanner;
        this.menuItems = menuItems;
    }

    public void run() {
        while(true) {
            showMenu();
            int choice = getChoiceUser();
            if(choice <= 0 || choice > menuItems.length) {
                System.out.println("Некорректный ввод");
                continue;
            }
            menuItems[choice - 1].execute();
            if (menuItems[choice - 1].isFinal()) break;
        }
    }

    private int getChoiceUser() {
        System.out.print("Сделайте Ваш выбор : ");
        int choice = scanner.nextInt();
        scanner.nextLine();
        return choice;
    }

    private void showMenu() {
        System.out.println("-----------------------------------   Меню   -----------------------------------");
        for (int index = 0; index < menuItems.length; index++) {
            System.out.printf("%2d - %s\n", index + 1, menuItems[index].getName());
        }
        System.out.println("--------------------------------------------------------------------------------");
    }
}