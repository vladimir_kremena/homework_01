package ua.ithillel.dnipro.kremena.task1.view.menu.imp;

import ua.ithillel.dnipro.kremena.task1.entity.Point;
import ua.ithillel.dnipro.kremena.task1.list.List;
import ua.ithillel.dnipro.kremena.task1.services.CircleService;
import ua.ithillel.dnipro.kremena.task1.services.PointService;
import ua.ithillel.dnipro.kremena.task1.view.menu.MenuItem;

public class ContainsPointMenuItem implements MenuItem {

    private PointService pointService;
    private CircleService circleService;

    public ContainsPointMenuItem(PointService pointService, CircleService circleService) {
        this.pointService = pointService;
        this.circleService = circleService;
    }

    @Override
    public String getName() {
        return "Показать точки лежащие в окружности";
    }

    @Override
    public void execute() {
        List containsPointList = circleService.getContainsPoint(circleService.get(), pointService.getAll());
        if (containsPointList.size() == 0) {
            System.out.println("В пределеах окружности точки отсутствуют");
        } else {
            System.out.println("Список точек, которые лежат в окружности : ");
            for (int index = 0; index < containsPointList.size(); index++) {
                System.out.printf("%2d - %s\n", index + 1, containsPointList.get(index));
            }
        }
    }
}