package ua.ithillel.dnipro.kremena.task1.view.menu.imp;

import ua.ithillel.dnipro.kremena.task1.entity.Circle;
import ua.ithillel.dnipro.kremena.task1.entity.Point;
import ua.ithillel.dnipro.kremena.task1.services.CircleService;
import ua.ithillel.dnipro.kremena.task1.view.menu.MenuItem;

import java.util.Scanner;

public class ChangeCircleMenuItem implements MenuItem {

    private Scanner scanner;
    private CircleService circleService;

    public ChangeCircleMenuItem(Scanner scanner, CircleService circleService) {
        this.scanner = scanner;
        this.circleService = circleService;
    }

    @Override
    public String getName() {
        return "Изменить параметры окружности";
    }

    @Override
    public void execute() {
        if (circleService.get() == null) {
            System.out.println("Окружность еще не создана. Создание окружности : ");
            circleService.addCircle(createCircle());
        } else {
            circleService.addCircle(circleModification());
        }
    }

    private Circle createCircle() {
        System.out.println("Введите координаты центра окружности : ");
        System.out.print("Координата x = ");
        double xCoordinate = (Double.parseDouble(scanner.next()));
        System.out.print("Координата y = ");
        double yCoordinate = (Double.parseDouble(scanner.next()));
        System.out.print("Введите радиус окружности : ");
        double radius = Double.parseDouble(scanner.next());
        return new Circle(new Point(xCoordinate, yCoordinate), radius);
    }

    private Circle circleModification() {
        System.out.println("Введите новые координаты центра окружности : ");
        System.out.print("Координата x = ");
        double xCoordinate = (Double.parseDouble(scanner.next()));
        System.out.print("Координата y = ");
        double yCoordinate = (Double.parseDouble(scanner.next()));
        System.out.print("Введите новый радиус окружности : ");
        double radius = Double.parseDouble(scanner.next());
        return new Circle(new Point(xCoordinate, yCoordinate), radius);
    }
}