package ua.ithillel.dnipro.kremena.task1.view.menu;

public interface MenuItem {

    String getName();

    void execute();

    default boolean isFinal() {
        return false;
    }

}