package ua.ithillel.dnipro.kremena.task1.view.menu.imp;

import ua.ithillel.dnipro.kremena.task1.entity.Point;
import ua.ithillel.dnipro.kremena.task1.services.PointService;
import ua.ithillel.dnipro.kremena.task1.view.menu.MenuItem;

import java.util.Scanner;

public class AddPointMenuItem implements MenuItem {

    private Scanner scanner;
    private PointService pointService;

    public AddPointMenuItem(Scanner scanner, PointService pointService) {
        this.scanner = scanner;
        this.pointService = pointService;
    }

    @Override
    public String getName() {
        return "Добавить точку";
    }

    @Override
    public void execute() {
        pointService.addPoint(createPoint());
        pointService.showAllPoint();
    }

    private Point createPoint() {
        System.out.println("Введите координаты точки : ");
        System.out.print("Координата x = ");
        double xCoordinate = (Double.parseDouble(scanner.next()));
        System.out.print("Координата y = ");
        double yCoordinate = (Double.parseDouble(scanner.next()));
        return new Point(xCoordinate, yCoordinate);
    }
}