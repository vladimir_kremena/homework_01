package ua.ithillel.dnipro.kremena.task1.view.menu.imp;

import ua.ithillel.dnipro.kremena.task1.view.menu.MenuItem;

public class ExitMenuItem implements MenuItem {

    @Override
    public String getName() {
        return "Выход";
    }

    @Override
    public void execute() {
        System.out.println("До скорых встреч");
    }

    @Override
    public boolean isFinal() {
        return true;
    }
}