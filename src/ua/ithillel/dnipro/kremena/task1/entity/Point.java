package ua.ithillel.dnipro.kremena.task1.entity;

public class Point {

    private double xCoordinate;

    private double yCoordinate;

    public Point() {}

    public Point(double xCoordinate, double yCoordinate) {
        this.xCoordinate = xCoordinate;
        this.yCoordinate = yCoordinate;
    }

    public double getxCoordinate() {
        return xCoordinate;
    }

    public void setxCoordinate(double xCoordinate) {
        this.xCoordinate = xCoordinate;
    }

    public double getyCoordinate() {
        return yCoordinate;
    }

    public void setyCoordinate(double yCoordinate) {
        this.yCoordinate = yCoordinate;
    }

    public double distanceToPoint(Point point) {
        return Math.sqrt((xCoordinate - point.xCoordinate) * (xCoordinate - point.xCoordinate) +
                (yCoordinate - point.yCoordinate) * (yCoordinate - point.yCoordinate));
    }

    @Override
    public String toString() {
        return  "Point [" +
                "xCoordinate = " + xCoordinate + " | " +
                "yCoordinate = " + yCoordinate +
                ']';
    }
}