package ua.ithillel.dnipro.kremena.task1.entity;

public class Circle {

    private Point center;
    private double radius;

    public Circle() {}

    public Circle(Point center, double radius) {
        this.center = center;
        this.radius = radius;
    }

    public Point getCenter() {
        return center;
    }

    public void setCenter(Point center) {
        this.center = center;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public boolean containsPoint(Point point) {
        double distance = center.distanceToPoint(point);
        return distance <= radius;
    }

    @Override
    public String toString() {
        return "Circle [" +
                "center = " + center + " | " +
                "radius = " + radius +
                ']';
    }
}