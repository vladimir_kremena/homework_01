package ua.ithillel.dnipro.kremena.task1.services;

import ua.ithillel.dnipro.kremena.task1.entity.Circle;
import ua.ithillel.dnipro.kremena.task1.list.List;
import ua.ithillel.dnipro.kremena.task1.list.impl.PointList;

public interface CircleService {

    void addCircle(Circle circle);

    Circle get();

    List getContainsPoint(Circle circle, PointList pointList);

    List getNotContainsPoint(Circle circle, PointList pointList);
}