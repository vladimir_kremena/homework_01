package ua.ithillel.dnipro.kremena.task1.services.imp;

import ua.ithillel.dnipro.kremena.task1.entity.Circle;
import ua.ithillel.dnipro.kremena.task1.list.List;
import ua.ithillel.dnipro.kremena.task1.list.impl.PointList;
import ua.ithillel.dnipro.kremena.task1.services.CircleService;

public class InMemoryCircleService implements CircleService {

    private Circle circle;

    @Override
    public void addCircle(Circle circle) {
        this.circle = circle;
    }

    @Override
    public Circle get() {
        return circle;
    }

    @Override
    public List getContainsPoint(Circle circle, PointList pointList) {
        List containsPointList = new PointList();
        for (int index = 0; index < pointList.size(); index++) {
            if (circle.containsPoint(pointList.get(index))) {
                containsPointList.add(pointList.get(index));
            }
        }
        return containsPointList;
    }

    @Override
    public List getNotContainsPoint(Circle circle, PointList pointList) {
        List notContainsPointList = new PointList();
        for (int index = 0; index < pointList.size(); index++) {
            if (!circle.containsPoint(pointList.get(index))) {
                notContainsPointList.add(pointList.get(index));
            }
        }
        return notContainsPointList;
    }
}