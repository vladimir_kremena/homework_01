package ua.ithillel.dnipro.kremena.task1.services.imp;

import ua.ithillel.dnipro.kremena.task1.entity.Point;
import ua.ithillel.dnipro.kremena.task1.list.impl.PointList;
import ua.ithillel.dnipro.kremena.task1.services.PointService;

public class InMemoryPointService implements PointService {

    private PointList pointList = new PointList();

    @Override
    public void addPoint(Point point) {
        pointList.add(point);
    }

    @Override
    public void showAllPoint() {
        for (int index = 0; index < pointList.size(); index++) {
            System.out.printf("%2d - %s\n", index + 1, pointList.get(index));
        }
    }

    @Override
    public PointList getAll() {
        return pointList;
    }

}