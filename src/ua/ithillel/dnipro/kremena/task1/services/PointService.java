package ua.ithillel.dnipro.kremena.task1.services;

import ua.ithillel.dnipro.kremena.task1.entity.Point;
import ua.ithillel.dnipro.kremena.task1.list.impl.PointList;

public interface PointService {

    void addPoint(Point point);

    void showAllPoint();

    PointList getAll();

}